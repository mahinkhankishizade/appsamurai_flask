from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class App(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(50), nullable=False)

    def __init__(self, token):
        self.token = token

    @property
    def serialize(self):
        return {
            "id": self.id,
            "token": self.token
        }


class StoryMetadata(db.Model):
    story_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    metadata_ = db.Column("metadata", db.JSON, nullable=False)
    app_id = db.Column(db.Integer, db.ForeignKey('app.id'), nullable=False)

    def __init__(self, metadata_, app_id):
        self.metadata_ = metadata_
        self.app_id = app_id

    @property
    def serialize(self):
        return {
            "id": self.story_id,
            "metadata": self.metadata_["img"]
        }
