from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from datetime import datetime

from models import (App, StoryMetadata)

from response import (
    bad_request,
    success,
    not_found
)


app = Flask(__name__)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)


@app.route('/')
def index():
    return success("Hello AppSamurai! Flask version: 1.0.1")


@app.route('/stories/<app_token>', methods=['GET'])
def get_story_metadata(app_token):
    if app_token:
        app = App.query.filter_by(token=app_token).first()
        if app:
            app = app.serialize
            story_metadata = StoryMetadata.query.filter_by(app_id=app["id"]).all()
            stories = []
            if story_metadata:
                for story in story_metadata:
                    stories.append(story.serialize)

            result = {}
            result["app_id"] = app["id"]
            result["ts"] = int(datetime.utcnow().timestamp())
            result["metadata"] = stories

            return success(result)
        else:
            return not_found('App does not exist')
    else:
        return bad_request('Missing required parameter')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
