# appsamurai_flask

Preliminary project implementation in Flask

## This project consists only from the first assignment of the preliminary.

Further assignments are done in the aiohttp implementation (see report for the link)

How to run the project:

0. Clone the project with 'git clone'
1. Import Postman collection to the Postman
2. Open the terminal
3. Run 'cd src'
4. Run 'docker-compose build'
5. Run 'docker-compose up'
6. Go to the Postman and request the 'index' to see if it works

If you do not use Postman then just go to http://0.0.0.0:5000/ 
